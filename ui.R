library(shiny)
shinyUI(fluidPage(
  img(src='logo.png', height="100", width="100",align = "right"),
  titlePanel("Projet Aspire"),
  tags$div(checked = NA,
           tags$a(href = "Exemple_Utilisation_ASPIRE.pdf", target="_blank","Notice d'utilisation")
  ),
  sidebarLayout(
    
    sidebarPanel(
      tags$a(href = "ASPIRE_tab_exemple_Projet.txt", target="_blank","Fichier exemple Projet"),
        fileInput('file1', 'Choisir le fichier Projet',
                accept=c('text/csv', 
                         'text/comma-separated-values,text/plain', 
                         '.csv')),
      tags$a(href = "ASPIRE_tab_exemple_Objectifs.txt", target="_blank","Fichier exemple Objectifs"),
      fileInput('file2', 'Choisir le fichier Objectif',
                accept=c('text/csv', 
                         'text/comma-separated-values,text/plain', 
                         '.csv')),
      tags$a(href = "ASPIRE_tab_exemple_Variables.txt", target="_blank","Fichier exemple Variables"),
      fileInput('file3', 'Choisir le fichier Variable',
                accept=c('text/csv', 
                         'text/comma-separated-values,text/plain', 
                         '.csv'))
      
      ),
    mainPanel(
      uiOutput("tb")
      
# use below code if you want the tabset programming in the main panel. If so, then tabset will appear when the app loads for the first time.
#       tabsetPanel(tabPanel("Summary", verbatimTextOutput("sum")),
#                   tabPanel("Data", tableOutput("table")))
      )
    )
))
