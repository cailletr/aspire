FROM gricad-registry.univ-grenoble-alpes.fr/osug/dc/shiny-base:4.0.2

# Install dependencies
RUN install.r \
      fmsb \
      knitr \ 
      && rm -rf /tmp/downloaded_packages

# Copy app files
COPY ASPIRE_V9.R server.R ui.R /srv/shiny/
